/**
 * Skills and abilities list
 */
var SKILLS = {
    "Guard Leech": "Increases power and condition damage by 20 (max stack of 5; stacks are lost on death).",
    "Applied Fortitude": "Gain 50 vitality per stack (max stack of 5; lose stacks on death)",
    "Spread Shot": "Fire a spread of bolts through foes. Does double damage to siege weapons.<br><br>Damage: 5,081 / 7,620 Superior<br>Number of Targets: 5<br>Range: 3,000",
    "Toxic Unveiling Short": "Removes stealth and applies poison to target<br><br>Damage (8x): 6,352 / 9,528 Superior<br>Poison: 2 seconds (1344 dmg)<br>Revealed: 2 seconds<br>Radius: 540<br>Range: 2,800",
    "Burning Shell": "Creates a fiery dome that reflects projectiles<br><br>Duration: 10 seconds<br>Range: 275",
    "Concussion Barrage": "Launch a barrage of rounds that push back foes where they land<br><br>Radius: 400<br>Number of Targets: 10",
    "Siege Bubble": "Form a protective bubble around the siege weapon and its user that destroys projectiles on contact<br><br>Radius: 450<br>Duration: 3 seconds",
    "Healing Oasis": "Fire a shot that creates an oasis of water that heals allies<br><br>Healing: 320<br>Radius: 420<br>Duration: 12 seconds<br>Combo Field: Water<br>Range: 10,000",
    "Iron Hide": "Iron Hide is an effect that reduces direct damage by 50%.",
    "Structural Vulnerability": "1% extra damage per stack",
    "Impact Slam": "Ram target with forceful attack. Does extra damage to gates and launches foes on the other side of the gate<br><br>Damage: 8,020 / 12,027 Superior<br>Launch: 450",
    "Iron Will": "Apply Iron Hide (50% damage reduction) to allies in an area.<br><br>Iron Hide (3s): -50% Incoming Damage<br>Number of Targets: 50<br>Radius: 300"
};

/**
 * WvW rank titles
 * Only actual for calculator are listed
 */
RANKS = {
    1: 'Invader',          5: 'Assaulter',         10: 'Raider',         15: 'Recruit',         20: 'Scout',         30: 'Soldier',         40: 'Squire',         50: 'Footman',          60: 'Knight',          70: 'Major',          80: 'Colonel',          90: 'General',         100: 'Veteran',         110: 'Champion',         120: 'Legend',
    150: 'Bronze Invader', 180: 'Bronze Assaulter', 210: 'Bronze Raider', 240: 'Bronze Recruit', 270: 'Bronze Scout', 300: 'Bronze Soldier', 330: 'Bronze Squire', 360: 'Bronze Footman',  390: 'Bronze Knight',  420: 'Bronze Major',  450: 'Bronze Colonel',  480: 'Bronze General',  510: 'Bronze Veteran',  540: 'Bronze Champion',  570: 'Bronze Legend',
    620: 'Silver Invader', 670: 'Silver Assaulter', 720: 'Silver Raider', 770: 'Silver Recruit', 820: 'Silver Scout', 870: 'Silver Soldier', 920: 'Silver Squire', 970: 'Silver Footman', 1020: 'Silver Knight', 1070: 'Silver Major', 1120: 'Silver Colonel', 1170: 'Silver General', 1220: 'Silver Veteran', 1270: 'Silver Champion', 1320: 'Silver Legend',
    1395: 'Gold Invader'
};

/**
 * Next property in object
 *
 * @param id
 * @returns {int}
 */
var nextRank = function(id){

    var next = 5;

    $.each(RANKS, function(index, value) {

        if( index > id ) {
            next = index;
            return false;
        }

    });

    return next;

};


/**
 * Get rank title
 *
 * @param rank {int}
 * @returns {string}
 */
var getRankTitle = function(rank) {

    var title = 'Invader';

    $.each(RANKS, function(index, value) {

        if( rank == index ) {
            title = value;
            return;
        }
        if( rank > index ) {
            title = value;
        }

    });

    return title;

};


/**
 * Load skill set from URI hash
 *
 * @returns {*}
 */
var loadFromHash = function() {

    var hash = document.location.hash.slice(1);

    if (hash.length % 2 == 0) {
        for (val = 0; val < hash.length; val += 2) {
            ability = parseInt(hash[val], 36);
            points  = parseInt(hash[val + 1], 36);
            $('td.points:eq(' + ability + ')').find('li').slice(0, points).trigger('click', true);
        }
    }

};


/**
 * Update data sets in the calculator
 * @param triggered {boolean}
 * @return void
 */
var update = function(triggered) {

    hash  = "";
    rank  = parseInt($('#rank').text(), 10);
    title = getRankTitle(rank);

    $("#next-rank").html( nextRank(rank) );
    $('#rank-title').text(title);
    $('#profit').empty();
    $('.gained-abilities').show();

    abilities = $('li.active').parents('td.points');

    $(abilities.toArray().reverse()).each(function (idx, elem) {

        // Generate hash
        if (!triggered) {
            ability = $(elem);
            nactive = $(elem).find('li.active').length;
            hash    += (ability.parent().index()).toString(36) + nactive.toString(36);
        }

        // Find ability title in current <tr>
        ability_title = $(this).closest('tr').find('div.ability-title').text();
        $('div#profit').append('<h4>' + ability_title + '</h4><ul></ul>');

        // Iterate current active skills
        $(this).find('li.active').each(function () {

            if ($(this).attr('data-group')) {
                group = $(this).data('group');
                if (!$(this).is('li.active[data-group=' + group + ']:last')) {
                    return true;
                }
            }

            title = $(this).data('title');
            match = title.match(/\[([\w\s]+)\]/);

            if (match) {
                title = title.replace(/\[([\w\s]+)\]/, "<abbr title=\"" + SKILLS[match[1]] + "\">$1</abbr>");
            }

            $('div#profit ul:last').append("<li>" + title + "</li>");
            return true;

        });

    });

    if( $('div#profit').text()=='' ) {
        $('div.gained-abilities').hide();
    }

    if (!triggered) {
        document.location.hash = hash;
    }

};


/**
 * Void main
 */
$( document ).ready(function() {


    /**
     * jQueryUI tooltip
     * Custom output support added to show tooltip with html contents
     */
    $( document ).tooltip({
        show: false,
        hide: false,
        position: {
            my: "center bottom-20",
            at: "center top"
        },
        content: function () {
            return $(this).prop('title');
        }
    });


    /**
     * Copy handler
     * Shows prompt with current location hash
     * @returns void
     */
    $("#copy").click(function(){
        prompt("Copy the link", location.href);
        return false;
    });


    /**
     * Full reset handler
     * Resets all values in calculator
     * @returns void
     */
    $("#reset").click(function(){
        $(".gained-abilities").hide();
        $('li.active').removeClass('active');
        $('li.maxed').removeClass('maxed');
        $('#rank').text('1');
        $('.spent').text('0');
        $('#next-rank').text('5');
        $('#rank-title').text('Invader');
        $('.skill-point-img').show();
        $('td.points').each(function() {
            $(this).find('div.amount').text($(this).find('li:first').data('value'));
        });
        document.location.hash = "";
        return false;
    });


    $('li').hover(

        /**
         * Hover event handler
         * @returns void
         */
        function() {
            $(this).addClass("hover");
            $(this).prevAll().addClass("hover");
        }, function() {
            $(this).removeClass("hover");
            $(this).prevAll().removeClass("hover");
        }

    ).click(

        /**
         * Click event handler
         * @param e
         * @param triggered
         */
        function(e, triggered) {

            // Processing
            rank    = $('#rank');
            points  = parseInt(rank.text(), 10) - 1;
            current = $(this).closest('td.points').find('div.amount');
            spent   = $(this).closest('td.points').find("div.spent > span");
            amount  = parseInt(spent.text(), 10);

            $(this).removeClass("maxed");
            $(this).siblings().removeClass("maxed");
            $(this).closest('td.points').find('div.point-sum').find('img').show();

            if (!$(this).hasClass('active')) {

                subtotal = parseInt($(this).attr("data-value"), 10);

                $(this).addClass("active");
                $(this).prevAll('li').each(function() {
                    if (!$(this).hasClass('active')) {
                        $(this).addClass('active');
                        subtotal += parseInt($(this).attr("data-value"), 10);
                    }
                });

                if($(this).nextAll().length == 0) {
                    $(this).addClass("maxed");
                    $(this).prevAll().addClass("maxed");
                    current.html('Maxed');
                    $(this).closest('td.points').find('div.point-sum').find('img').hide();
                }

                rank.text(points + subtotal + 1);
                current.text($(this).next('li').data('value'));
                spent.text(amount + subtotal);

            }
            else {

                nextPt = $(this).nextAll('li:first');

                if (nextPt) {

                    // Only first point in the ability branch left active
                    if (nextPt.hasClass('active')) {
                        subtotal = 0;
                        current.text($(this).next('li').data('value'));
                    }
                    // No active point in current branch
                    else {
                        $(this).removeClass('active');
                        subtotal = parseInt($(this).attr("data-value"), 10);
                        current.text($(this).data('value'));
                    }

                }
                else {
                    $(this).removeClass('active');
                    var subtotal = parseInt($(this).attr("data-value"), 10);
                }

                $(this).nextAll('li').each(function () {
                    if ($(this).hasClass('active')) {
                        subtotal += parseInt($(this).attr("data-value"), 10);
                        $(this).removeClass('active');
                    }
                });

                rank.text(points - subtotal + 1);
                spent.text(amount - subtotal);

            }

            update(triggered);
            e.preventDefault();

        }

    );


    /**
     * Load calculator data from hash string
     */
    loadFromHash();

});