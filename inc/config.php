<?php

return array(

    /**
     * Abilities information
     */
    'abilities' => array(

        array(
            'title'       => 'Siege Golem Mastery',
            'description' => 'Improves Siege Golem skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increases your piloted golem’s movement speed for 15 seconds after destroying a gate or a wall'),
                2 => array(10, 'Increases your piloted golem’s base power and precision by 15%'),
                3 => array(15, 'The shield bubble now also heals nearby allies'),
                4 => array(20, 'Increases your piloted golem’s base vitality and toughness by 15%'),
                5 => array(25, 'Your piloted golem will safely eject you once it’s defeated'),
            ),
        ),

        array(
            'title'       => 'Guard Killer',
            'description' => 'Increases damage to guards',
            'total'       => 115,
            'points'      => array(
                1  => array(1,  '1% damage to guards', 'gr1'),
                2  => array(2,  '2% (total) damage to guards', 'gr1'),
                3  => array(3,  '3% (total) damage to guards', 'gr1'),
                4  => array(4,  '4% (total) damage to guards', 'gr1'),
                5  => array(5,  '5% (total) damage to guards', 'gr1'),
                6  => array(10, 'All guard killer bonuses now also affect enemy lord and supervisors'),
                7  => array(15, '7% (total) damage to guards, lords and supervisors', 'gr1'),
                8  => array(20, 'Gain 50% of your endurance back after killing guards, lords and supervisors'),
                9  => array(25, '10% (total) damage to guards, lords and supervisors', 'gr1'),
                10 => array(30, 'Add 1 stack of [Guard Leech] on killing a guard, lord or supervisor<br>Guard Leech: Increases power and condition damage by 20 (max stack of 5, stacks are lost on death)'),
            ),
        ),

        array(
            'title'       => 'Defence Against Guards',
            'description' => 'Reduces the damage you take from enemy guards',
            'total'       => 115,
            'points'      => array(
                1  => array(1,  '1% reduced damage from guards', 'gr2'),
                2  => array(2,  '2% reduced damage (total) from guards', 'gr2'),
                3  => array(3,  '3% reduced damage (total) from guards', 'gr2'),
                4  => array(4,  '4% reduced damage (total) from guards', 'gr2'),
                5  => array(5,  '5% reduced damage (total) from guards', 'gr2'),
                6  => array(10, 'All Defense against Guard abilities count against lords and supervisors'),
                7  => array(15, '7% reduced damage (total) against guards, lords, and supervisors', 'gr2'),
                8  => array(20, 'Gain Retaliation for 10 seconds when you negate damage from guards, lords, or supervisors This effect can trigger only once every 20 seconds.'),
                9  => array(25, '10% reduced damage (total) against guards, lords, and supervisors', 'gr2'),
                10 => array(30, 'Gain 1 stack of [Applied Fortitude] when killing a guard, lord, or supervisor<br>Applied Fortitude: Each stack grants 50 vitality (max of 5 stacks, stacks are lost on death)'),
            ),
        ),

        array(
            'title'       => 'Mercenary\'s Bane',
            'description' => 'Increases damage to and reduces the damage from mercenaries',
            'total'       => 15,
            'points'      => array(
                1 => array(1, '1% increase to damage and defense against mercenaries', 'gr3'),
                2 => array(2, '2% (total) increase to damage and defense against mercenaries', 'gr3'),
                3 => array(3, '3% (total) increase to damage and defense against mercenaries', 'gr3'),
                4 => array(4, '4% (total) increase to damage and defense against mercenaries', 'gr3'),
                5 => array(5, '5% (total) increase to damage and defense against mercenaries', 'gr3'),
            ),
        ),

        array(
            'title'       => 'Siege Bunker',
            'description' => 'Reduces the siege damage you take',
            'total'       => 30,
            'points'      => array(
                1 => array(2,  '1% increased siege defense', 'gr4'),
                2 => array(4,  '2% (total) increased siege defense', 'gr4'),
                3 => array(6,  '3% (total) increased siege defense', 'gr4'),
                4 => array(7,  '4% (total) increased siege defense', 'gr4'),
                5 => array(10, '5% (total) increased siege defense', 'gr4'),
            ),
        ),

        array(
            'title'       => 'Siege Might',
            'description' => 'Increases damage done with siege-engine attacks',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  '1% increased siege damage', 'gr5'),
                2 => array(10, '2% (total) increased siege damage', 'gr5'),
                3 => array(15, '3% (total) increased siege damage', 'gr5'),
                4 => array(20, '4% (total) increased siege damage', 'gr5'),
                5 => array(25, '5% (total) increased siege damage', 'gr5'),
            ),
        ),

        array(
            'title'       => 'Supply Capacity',
            'description' => 'Increases the amount of supply you can carry',
            'total'       => 300,
            'points'      => array(
                1 => array(20,  'Increases supply capacity by 1', 'gr6'),
                2 => array(40,  'Increases supply capacity by 2 (total)', 'gr6'),
                3 => array(60,  'Increases supply capacity by 3 (total)', 'gr6'),
                4 => array(80,  'Increases supply capacity by 4 (total)', 'gr6'),
                5 => array(100, 'Increases supply capacity by 5 (total)', 'gr6'),
            ),
        ),

        array(
            'title'       => 'Cannon Mastery',
            'description' => 'Improve cannon use',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Basic fire removes boons'),
                2 => array(10, 'Increases cannon damage by 27%'),
                3 => array(15, 'Grapeshot does more bleed damage'),
                4 => array(20, 'Increases all blast radius'),
                5 => array(25, 'Increases chill duration'),
            ),
        ),

        array(
            'title'       => 'Ballista Mastery',
            'description' => 'Improve ballista skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  '[Spread Shot] - Fires projectiles in a cone pattern'),
                2 => array(10, 'Increases ballista damage by 25%'),
                3 => array(15, 'Fires faster bolts'),
                4 => array(20, 'Increases ballista range'),
                5 => array(25, 'Increases Shatter Shot radius'),
            ),
        ),

        array(
            'title'       => 'Arrow Cart Mastery',
            'description' => 'Improve arrow cart skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increases range on all arrow cart skills'),
                2 => array(10, 'Increases damage on all arrow cart skills'),
                3 => array(15, 'Increases effectiveness of applied conditions'),
                4 => array(20, 'Increases radius on all arrow cart skills'),
                5 => array(25, '[Toxic Unveiling Shot] - Removes stealth and applies poison to targets'),
            ),
        ),

        array(
            'title'       => 'Burning oil mastery',
            'description' => 'Improves all Burning Oil skills and unlocks new abilities',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increases Burning Oil radius by 25%'),
                2 => array(10, 'Increases Burning Oil damage by 15%'),
                3 => array(15, 'While using the oil, reduces the damage received by the oil pot and the player'),
                4 => array(20, 'Basic Burning Oil attacks burn one supply from target'),
                5 => array(25, '[Burning Shell] - Creates a fiery dome that reflects projectiles'),
            ),
        ),

        array(
            'title'       => 'Mortar Mastery',
            'description' => 'Improve mortar skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increase blast radius on all mortar skills'),
                2 => array(10, 'Increases mortar burn field duration'),
                3 => array(15, 'Reduce the recharge of all mortar skills'),
                4 => array(20, 'Increase damage on all mortar skills by 25%'),
                5 => array(25, '[Concussion Barrage] - Launch a barrage of rounds that push back foes where they land'),
            ),
        ),

        array(
            'title'       => 'Build Master',
            'description' => 'Improves you building capabilities',
            'total'       => 30,
            'points'      => array(
                1 => array(1,  'Spend 4 supply at a time when building siege', 'gr7'),
                2 => array(4,  'Spend 6 supply at a time when building siege', 'gr7'),
                3 => array(10, 'Spend 8 supply at a time when building siege', 'gr7'),
                4 => array(15, 'Spend 10 supply at a time when building siege', 'gr7'),
            ),
        ),

        array(
            'title'       => 'Repair Master',
            'description' => 'Improves you repairing capabilities',
            'total'       => 20,
            'points'      => array(
                1 => array(1,  'Spend 4 supply at a time when repairing', 'gr8'),
                2 => array(3,  'Spend 6 supply at a time when repairing', 'gr8'),
                3 => array(6,  'Spend 8 supply at a time when repairing', 'gr8'),
                4 => array(10, 'Spend 10 supply at a time when repairing', 'gr8'),
            ),
        ),

        array(
            'title'       => 'Catapult Mastery',
            'description' => 'Improves catapult skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increases damage done to walls by 20%'),
                2 => array(10, 'Gravel Shot now applies bleeding'),
                3 => array(15, 'Increases radius of all catapult skills'),
                4 => array(20, 'Increases catapult damage by 25%'),
                5 => array(25, '[Siege Bubble] - Creates a dome that destroys projectiles on impact'),
            ),
        ),

        array(
            'title'       => 'Supply Master',
            'description' => 'Improve your ability in gathering, spending, and running supply',
            'total'       => 15,
            'points'      => array(
                1 => array(1, 'Pick up supply faster'),
                2 => array(2, 'Gain swiftness after spending all of your supply'),
                3 => array(3, 'Gain improved swiftness when picking up supply'),
                4 => array(4, 'Have a chance to recover spent supply when spending supply'),
                5 => array(5, 'Deployed siege sites are invulnerable for a short duration'),
            ),
        ),

        array(
            'title'       => 'Trebuchet Mastery',
            'description' => 'Improve trebuchet skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Increases the damage on trebuchet skills'),
                2 => array(10, 'Cow ammo now drains supply from enemy players'),
                3 => array(15, 'Increases the radius of trebuchet skills'),
                4 => array(20, '[Healing Oasis] - Creates a water field that heals allies'),
                5 => array(25, 'Basic shots that hit enemy supply depots will destroy some of the objective\'s supply'),
            ),
        ),

        array(
            'title'       => 'Flame Ram Mastery',
            'description' => 'Improve flame ram skills',
            'total'       => 75,
            'points'      => array(
                1 => array(5,  'Gain Iron Hide while interacting with the flame ram'),
                2 => array(10, 'Flame Blast now applies Structural Vulnerability to gates'),
                3 => array(15, 'Reduce recharge of flame ram skills'),
                4 => array(20, '[Impact Slam] - Launch enemies on the other side of the door'),
                5 => array(25, '[Iron Will] - Support skill that applies Iron Hide to nearby allies'),
            ),
        ),


    ),

);