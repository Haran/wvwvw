<?php

class WvW
{

    public $abilities;


    /**
     * @ignore
     */
    public function __construct()
    {
        $config = require_once('config.php');
        $this->abilities = $config['abilities'];
    }


    /**
     * Retrieve path to an image of particular skill
     * @param $title string
     * @return string
     */
    function getImage( $title )
    {
        return 'images/' . str_replace(array(" ", "'"), '_', strtolower($title)) . '.png';
    }

}
