<!DOCTYPE html>
<html>
<head>
    <title>WvW Ability calculator</title>
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/calc.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <?php
        require_once('inc/wvw.php');
        $wvw = new WvW;
    ?>

    <div class="grid grid-pad">

        <!-- Left column -->
        <div class="col-9-12">
            <div class="calculator-container">

                <div class="logo">
                    <img src="images/wvw.png" alt="WvW">
                    <span>World vs. World Rank and Abilities Calculator</span>
                    <div class="decorative-skill-up"></div>
                </div>
                <div class="rank">
                    Rank <span id="rank">1</span> : <span id="rank-title">Invader</span>
                </div>
                <div class="next-title">
                    Next title at Rank <span id="next-rank">5</span>
                </div>
                <div class="links">
                    <a href="#" id="copy">Link</a>
                    <a href="#" id="reset">Reset</a>
                </div>
                <div style="border: 1px solid #222222; border-top: 0; border-bottom: 0">
                    <table class="calculator-layout">
                        <thead>
                        <tr>
                            <th colspan="3">Abilities</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($wvw->abilities as $aid => $ability): ?>
                            <tr>
                                <td class="ability-icon"><?php echo '<img alt="'.$ability['title'].'" src="'.$wvw->getImage($ability['title']).'">'; ?></td>
                                <td class="title">
                                    <div class="ability-title"><?php echo $ability['title']; ?></div>
                                    <div class="ability-description"><?php echo $ability['description']; ?></div>
                                </td>
                                <td class="points">
                                    <div class="point-sum">
                                        <div class="amount"><?php echo $ability['points'][1][0] ?></div><img src="images/skill-up-small.png" alt="Skill points" class="skill-point-img">
                                    </div>
                                    <ul>
                                        <?php foreach($ability['points'] as $pid => $point): ?>
                                            <?php
                                                $tooltip    = $point[1];
                                                $data_title = current(explode("<br>", $point[1]));
                                                $data_group = (array_key_exists(2, $point)) ? 'data-group="'.$point[2].'"' : '';
                                            ?>
                                            <li title="<?php echo $tooltip; ?>" data-value="<?php echo $point[0] ?>" <?php echo $data_group ?> data-title="<?php echo $data_title; ?>"><div class="point"></div></li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="spent"><span title="Total points spent on <?php echo $ability['title']; ?>">0</span></div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="copyright">
                    &copy; 2014 <a href="http://unitteam.ru">Unit Team</a>. Code based on <a href="http://howle.org" target="_blank">ignite</a> sources<br>
                    All images are the copyright of <a href="http://www.guildwars2.com/" target="_blank">ArenaNet</a> and respective owners
                </div>

            </div>
        </div>

        <!-- Right column -->
        <div class="col-3-12 gained-abilities">
            <div class="content subtitle">Profit</div>
            <div class="content" id="profit"></div>
        </div>

    </div>

</body>
</html>
