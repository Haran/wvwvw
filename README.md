Description
===========

This is a WvW ability point calculator for the Guild Wars 2. You can
plan points investment and save links for the created build.

Credits
--------

Based on code by *ignite* from http://howle.org

License
--------

As long as it's impossible to get in touch with original version author
it's decided to release under the most comfortable permissive license -
MIT license.